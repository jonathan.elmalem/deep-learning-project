# Deep Learning Project

MSc IA 2020-2021

Authored by Jonathan Elmalem and Paul Dampierre

Due to recent events around the world that featured large gatherings of people in strikes and demonstrations, crowd management and monitoring has become a cruicial tool to maintain law and order. Multiple research efforts have been made to further develop crowd counting techniques. This task poses many challenges including density variation, irregular distribution of objects and occlusion.

In this project we aim to reproduce two popular architectures that solve this problem and lead a comparitive study between the SS-DCnet architecture and the MCNN architecture.

The papers are
* MCNN ["Single-Image Crowd Counting via Multi-Column Convolutional Neural Network"](https://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Zhang_Single-Image_Crowd_Counting_CVPR_2016_paper.pdf)
* SS-DCNet: ["From Open Set to Closed Set: Supervised Spatial Divide-and-Conquer for Object Counting"](https://arxiv.org/pdf/2001.01886.pdf)

## Environment
Install required packages according to `requirements.txt` by running 

```Shell
pip install -r requirements.txt
```

## Datasets

The dataset used to train and test our model was the ShanghaiTech dataset which contains highly dense images of crowd in Shanghai (part B) as well as images of crowd taken from the internet (part A). You can download it from [this repo](https://github.com/desenzhou/ShanghaiTechDataset) or from [kaggle](https://www.kaggle.com/tthien/shanghaitech). After unpacking the archive, you will have the following directory structure:

```
./
└── MCNN/
└── SS-DCNet/
└── ShanghaiTech/
    ├── part_A/
    │   ├── test_data/
    │   │   ├── ground-truth/GT_IMG_{1,2,3,...,182}.mat
    │   │   └── images/IMG_{1,2,3,...,182}.jpg
    │   └── train_data/
    │       ├── ground-truth/GT_IMG_{1,2,3,...,300}.mat
    │       └── images/IMG_{1,2,3,...,300}.jpg
    └── part_B/
        ├── test_data/
        │   ├── ground-truth/GT_IMG_{1,2,3,...,316}.mat
        │   └── images/IMG_{1,2,3,...,316}.jpg
        └── train_data/
            ├── ground-truth/GT_IMG_{1,2,3,...,400}.mat
            └── images/IMG_{1,2,3,...,400}.jpg
```


## MCNN Approach

To run our MCNN implementation, you only need to run all the cells as it is a notebook.

## SS-DCNet Approach

Due to the complexity of the model and limited time to do it, we were largely inspired by this implementation [repo](https://github.com/dmburd/S-DCNet)

#### STEP 1 : Density Maps Generation

In order to generate the ground truth density maps you need to run the command line bellow 

```Shell
python ./SS-DCnet/gen_density_maps.py
```

Files with the names `density_maps_part_{A|B}_{train,test}.npz` will appear in the current directory.

#### STEP 2 : Training

In order to train the model you need to run the command line bellow 

```Shell
python ./SS-DCnet/train.py dataset=ShanghaiTech_part_B
# and/or similarly for part_A
```

Fine-tuning is supported (check the option `train.pretrained_ckpt`).

The logs and checkpoints generated during training are placed to a folder named like `outputs/<launch_date>/<launch_time>`. 

## Evaluation
`evaluate.py` is the script for evaluating a checkpoint. Select a checkpoint for epoch `N` and run a command like this:

```Shell
python ./SS-DCnet/evaluate.py \
    dataset=ShanghaiTech_part_B\
    test.trained_ckpt_for_inference=outputs/<date>/<time>/checkpoints/epoch_<N>.pth
```

You can get the checkpoint for 800 epochs for ShanghaiTech part A on [link_A](https://centralesupelec-my.sharepoint.com/:u:/g/personal/jonathan_elmalem_student-cs_fr/Ec41LwTgLsVJqlpvJXpBDJ4BYYFgbRZIyf0Sn486aLsiSw?e=kWOjor) and part B on [link_B](https://centralesupelec-my.sharepoint.com/:u:/g/personal/jonathan_elmalem_student-cs_fr/EfexH09Yi5tBqHQvaLiuIUsBT5Bj44uTSJSpLub1ebnVnw?e=yuJaWd)
